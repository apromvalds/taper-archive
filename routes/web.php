<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return redirect(app()->getLocale());
});

Auth::routes();
Route::prefix('{lang?}')->middleware('locale')->group(function() {
    Route::get('/', function () {
        return view('welcome');
    });


    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/posts/{type}', ['uses' => 'PostController@indexOfType', 'as' => 'posts.type']);
        Route::post('/posts/{type}/search', ['uses' => 'SearchController@filter','as' => 'posts.search']);
    Route::get('/post/{id}', ['uses' => 'PostController@indexSingle', 'as' => 'posts.single']);

    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
        Route::post('login', 'Auth\LoginController@login')->name('login');
    Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
//    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
//    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');


    Route::group(['middleware' => ['auth']], function () {

        Route::get('/admin', ['uses' => 'AdminController@index', 'as' => 'admin']);
        Route::delete('/admin/deleteuser/{id}', ['uses' => 'AdminController@delete']);

        Route::get('/post/{id}/destroy', ['uses' => 'PostController@destroy', 'as' => 'posts.destroy']);
        Route::get('/post/{id}/edit', ['uses' => 'PostController@edit', 'as' => 'posts.edit']);
            Route::patch('/post/{id}/edit', ['uses' => 'PostController@submitChanges']);
        Route::get('/post-new/{type}', 'PostController@add')->name('posts.add');
            Route::post('/post-new', ['uses' => 'PostController@store']);
    });
});