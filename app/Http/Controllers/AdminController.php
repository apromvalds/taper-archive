<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\User;

class AdminController extends Controller
{
    public function index($locale){
        $user = Auth::user();
        $users = User::latest()->paginate(5);
        if ($user && $user->hasRole('admin') ) {
            return view('auth.admin', ['locale' => $locale, 'users' => $users]);
        }
    }
    public function delete($locale, $id){
        $user = Auth::user();

        if ($user && $user->hasRole('admin') ) {
            foreach (Post::where('user_id', $id)->get() as $user_post){
                $user_post->delete();
            }
            User::findOrFail($id)->delete();
        }
        return redirect()->route('admin', ['locale' => $locale]);

    }
}
