<?php

namespace App\Http\Controllers;

use App\PostTypeTranslation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image as Image;
use App\Post;
use App\PostType;
use App\PostImage;

class PostController extends Controller
{
    /**
     * Display all posts of type
     */
    public function indexOfType($locale, $type)
    {
        $post_type = PostType::where('slug', $type)->first();
        $post_type_id = $post_type->id;
        $posts = Post::where('type', $post_type_id)->latest()->paginate(5);

        return view('posts.posts')->with('posts', $posts)->with('post_type', $post_type)->with('locale', $locale);
    }

    /**
     * Display a single post
     */
    public function indexSingle($locale, $id)
    {
        app()->setLocale($locale);
        $post = Post::findOrFail($id);
        $images = PostImage::where('post_id', $post->id)->get();
        return view('posts.single-post')->with('post', $post)->with('images', $images);
    }

    /**
     * Open new post editor form with required fields based on post type
     */
    public function add($locale, $type)
    {
        $type = PostType::where('id', $type)->first();
        return view('posts.add-post')->with('type', $type);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store($locale, Request $request)
    {
        $validatedData = $request->validate([
            'title' => 'required|max:255',
            'type_specific_fields' => 'required',
        ]);
        $input = $request->all();
        $input['user_id'] = auth()->user()->id;

        //handling wysiwyg images in summernote content
        $detail=$request->content;

        if ($detail) :
            $dom = new \domdocument();
            $dom->loadHtml($detail, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
            $images = $dom->getelementsbytagname('img');

            foreach($images as $k => $img){
                $data = $img->getattribute('src');

                list($type, $data) = explode(';', $data);
                list(, $data)      = explode(',', $data);

                $data = base64_decode($data);
                $image_name= time().$k.'.png';
                $path = public_path() .'/uploads/'. $image_name;

                file_put_contents($path, $data);

                $img->removeattribute('src');
                $img->setattribute('src', '/uploads/'.$image_name);
            }

            $detail = $dom->savehtml();
            $detail = str_replace(array('{','}'), '',$detail);
            $input['content'] = $detail;
        endif;
        //create post from input
        $post = Post::create($input);

        //create post images
        if($request->images){
            foreach($request->images as $photo){
                $name = time(). '_' . $photo->getClientOriginalName();

                \Image::make($photo)
                    ->resize(600, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })
                    ->save(public_path('uploads/').$name);

                PostImage::create([
                    'post_id'       => $post->id,
                    'path'          => '/uploads/' . $name,
                    'default'       => $photo,
                ]);
            }
        }

        $post->setDefaultLocale($locale);
        //add translatable fields as translations
        $post->translateOrNew($locale)->title = $input['title'];
        $post->translateOrNew($locale)->content = clean($input['content']);
        $post->save();

        return redirect()->route('posts.single', ['locale' => $locale, 'id' => $post->id ]);
    }

    /**
     * Open post editor form with required fields based on post type
     */
    public function edit($locale, $id)
    {
        $post = Post::findOrFail($id);
        $type = $post->post_type;
        $user = Auth::user();

        if ($user && $post->user_id == $user->id || $user->hasRole('admin') ) {
            return view('posts.edit-post')->with('post', $post)->with('type', $type);
        } else {
            return redirect()->back()->withErrors(__('posts.not-authorized-to-edit'));
        }
    }

    /**
     * Open post editor form with required fields based on post type
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function submitChanges($locale, Request $request, $id)
    {
        $post = Post::findOrFail($id);
        $type = $post->post_type;
        $user = Auth::user();

        $request->validate([
            'title' => 'required|max:255',
        ]);

        if ($user && $post->user_id == $user->id || $user->hasRole('admin') ) {
            $post->update(array('type_specific_fields' => $request['type_specific_fields']));

            $request['content'] = str_replace(array('{','}'), '',$request['content']);

            $post->translateOrNew($locale)->title = $request['title'];
            $post->translateOrNew($locale)->content = clean($request['content']);
            $post->save();
        } else {
            return redirect()->route('posts.edit', ['locale' => $locale, 'id' => $post->id])->with('error', 'Cannot edit post');
        }

        //create post images
        if($request->images){
            foreach($request->images as $photo){
                $name = time(). '_' . $photo->getClientOriginalName();

                \Image::make($photo)
                    ->resize(600, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })
                    ->save(public_path('uploads/').$name);

                PostImage::create([
                    'post_id'       => $post->id,
                    'path'          => '/uploads/' . $name,
                    'default'       => $photo,
                ]);
            }
        }

        return redirect()->route('posts.edit', ['locale' => $locale, 'id' => $post->id]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($locale, $id)
    {
        $post = Post::findOrFail($id);
        $user = Auth::user();



        if ($user && $post->user_id == $user->id || $user->hasRole('admin') ) {
            if($post->photos){
                foreach($post->photos as $photo){
                    $photoPath = public_path().$photo->path;

                    $photo->delete();
                    @unlink($photoPath);
                }
            }

            $post->deleteTranslations($post->getTranslationsArray());
            $post->delete();

            return redirect('/'.$locale.'/home');
        } else {
            return redirect()->route('posts.single', ['locale' => $locale, 'id' => $post->id])->with('post', $post)->with('images', $images)->withErrors(__('posts.not-authorized-to-delete'));
        }
    }
}