<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PostType;
use App\Post;

class SearchController extends Controller
{
    public function filter($locale, $type, Request $request, Post $post){
        $type_object = PostType::where('slug', $type)->first();
        $filterable = $post->where('type', $type_object->id)->latest()->get();
        $available_filters = json_decode($type_object->type_specific_fields);

        $filtered_ids = [];
        $match = false;
        $search_string =$request->search;
        foreach ($filterable as $filtered){
            $post_specific_values = json_decode($filtered->type_specific_fields);

            $match = true;
            if (isset($search_string)){
                if (stripos($filtered->title, $search_string) === false && stripos($filtered->content, $search_string) === false){
                    $match = false;
                }
            }
            foreach ($request->all() as $key => $value){
                if ($value && !empty($available_filters->$key)) {

                    if ( !isset($post_specific_values->$key) || $post_specific_values->$key != $value ){
                        $match = false;
                        break;
                    }
                }
            }

            $match == false ?: array_push($filtered_ids, $filtered->id);
        }

        $posts = Post::whereIn('id', $filtered_ids)->latest()->paginate(5);
        app()->setLocale($locale);
        if (!empty($filtered_ids)){
            return view('posts.posts')->with('post_type', $type_object)->with('posts', $posts)->with('search_inputs', $request->all())->with('locale', $locale);
        } else {
            return view('posts.posts')->with('post_type', $type_object)->with('posts', $posts)->with('search_inputs', $request->all())->with('locale', $locale)->withErrors([__('posts.not-found')]);
        }
    }
}
