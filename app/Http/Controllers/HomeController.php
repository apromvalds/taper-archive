<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
    $request->user()->authorizeRoles(['user', 'admin']);

    $posts = Post::where('user_id', $request->user()->id)->get();

    return view('home')->with('posts', $posts);
    }

}
