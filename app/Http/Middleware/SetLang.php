<?php
namespace App\Http\Middleware;

use Auth;
use Closure;
use App;

class SetLang {
    public function handle($request, Closure $next) {
        if(empty($request->has('lan'))) {
            if (Auth::user()) {
                Auth::user()->language = $request->input('lan');
                Auth::user()->save(); // this will do database UPDATE only when language was changed
            }
            App::setLocale($request->input('lan'));
        } else if (Auth::user()) {
            App::setLocale(Auth::user()->language);
        }

        return $next($request);
    }
}