<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Dimsav\Translatable\Translatable;

class Post extends Model
{
    protected $fillable = [
        'user_id', 'type', 'type_specific_fields'
    ];
    public $translatedAttributes = ['title', 'content'];

    use Translatable;

    public $translationModel = 'App\PostTranslation';

    function post_type(){
        return $this->belongsTo(PostType::class, 'type');
    }

    public function photos()
    {
        return $this->hasMany('App\PostImage', 'post_id', 'id');
    }
}