<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Dimsav\Translatable\Translatable;

class PostType extends Model
{
    protected $fillable = [
        'type', 'type_specific_fields', 'slug'
    ];

    public $translatedAttributes = ['title', 'description'];

    use Translatable;

    public $translationModel = 'App\PostTypeTranslation';

    /**
     * Return type specific field from slug name
     */
    function get_type_specific_field($slug){
        $all_fields = json_decode($this->type_specific_fields);

        if ($all_fields->$slug){
            return $all_fields->$slug;
        } else {
            return false;
        }
    }
}