<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostImage extends Model
{
    protected $fillable = [
        'post_id','path', 'default'
    ];

    protected $table = 'post_photos';
    public $timestamps = false;


    public function post()
    {
        return $this->belongsTo('App\Post', 'id', 'post_id');
    }
}
