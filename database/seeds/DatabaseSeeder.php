<?php
use Illuminate\Database\Seeder;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleTableSeeder::class);

        $this->call(UserTableSeeder::class);

        $this->call(PosttypeTableSeeder::class);

        $this->call(PostTableSeeder::class);

        $this->call(PosttypeTranslationsSeeder::class);

//        $this->call(PostTranslationsTableSeeder::class);
    }
}