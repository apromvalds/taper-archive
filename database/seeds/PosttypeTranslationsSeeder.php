<?php

use Illuminate\Database\Seeder;
use App\PostTypeTranslation;

class PosttypeTranslationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rods = new PostTypeTranslation();
        $rods->post_type_id = 1;
        $rods->locale = 'en';
        $rods->title = 'rods';
        $rods->description = 'Flyrod tapers and descriptions';
        $rods->save();

        $rods = new PostTypeTranslation();
        $rods->post_type_id = 1;
        $rods->locale = 'et';
        $rods->title = 'ridvad';
        $rods->description = 'Ridvad ja nende kirjeldused';
        $rods->save();

        $flies = new PostTypeTranslation();
        $flies->post_type_id = 2;
        $flies->locale = 'en';
        $flies->title = 'flies';
        $flies->description = 'Fly descriptions';
        $flies->save();

        $flies = new PostTypeTranslation();
        $flies->post_type_id = 2;
        $flies->locale = 'et';
        $flies->title = 'putukad';
        $flies->description = 'Putukad ja nende kirjeldused';
        $flies->save();
    }
}
