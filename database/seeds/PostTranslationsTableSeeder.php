<?php

use Illuminate\Database\Seeder;
use App\Post;

class PostTranslationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $post = new \App\PostTranslation();
        $post->post_id = 2;
        $post->locale = 'et';

        $post->title = 'Testpostitus';
        $post->content = 'testpostituse sisu';
        $post->save();

        $post2 = new \App\PostTranslation();
        $post2->post_id = 2;
        $post2->locale = 'en';

        $post2->title = 'Test post';
        $post2->content = 'test post content';
        $post2->save();
    }
}