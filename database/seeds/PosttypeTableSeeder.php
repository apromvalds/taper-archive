<?php

use Illuminate\Database\Seeder;
use App\PostType;

class PosttypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rods = new PostType();
        $rods->slug = 'rods';
        $rods_specific = array(
            'geometry' => array(
                'name' => 'Geometry',
                'filterable' => true,
                'type' => 'select',
                'options' => array(
                    'hex' => 'Hex',
                    'quad' => 'Quad',
                    'penta' => 'Penta'
                )
            ),
            'pieces' => array(
                'name' => 'Pieces',
                'filterable' => true,
                'type' => 'select',
                'options' => array(
                    '1' => '1',
                    '2' => '2',
                    '3' => '3',
                    '4' => '4'
                )
            ),
            'line-type' => array(
                'name' => 'Line type',
                'filterable' => true,
                'type' => 'select',
                'options' => array(
                    'DT' => 'DT',
                    'WF' => 'WF'
                )
            ),
            'line-weight' => array(
                'name' => 'Line Weight',
                'filterable' => true,
                'type' => 'select',
                'options' => array(
                    '3/0' => '3/0',
                    '2/0' => '2/0',
                    '0' => '0',
                    '1' => '1',
                    '2' => '2',
                    '3' => '3',
                    '4' => '4',
                    '5' => '5',
                    '6' => '6',
                    '7' => '7',
                    '8' => '8',
                    '9' => '9',
                    '10' => '10',
                    '11' => '11',
                    '12' => '12'
                )
            ),
            'rod-length' => array(
                'name' => 'Rod Length (inches)',
                'filterable' => true,
                'type' => 'number',
            ),
            'action-length' => array(
                'name' => 'Action Length',
                'filterable' => false,
                'type' => 'text',
            ),
            'stations' => array(
                'name' => 'Stations',
                'filterable' => false,
                'type' => 'stations-table',
            )
        );
        $rods->type_specific_fields = json_encode($rods_specific);
        $rods->save();
        $flies = new PostType();
        $flies->slug = 'flies';
        $flies->save();
    }
}
