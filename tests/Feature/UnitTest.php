<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UnitTest extends TestCase
{
    public function testIndex()
    {
        // test redirect when no language is set
        $response = $this->get('/');
        $response->assertStatus(302);

    }
}
