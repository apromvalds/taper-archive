<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Meie andmebaasis pole selliseid andmeid.',
    'throttle' => 'Liiga palju ebaõnnestunud sisselogimiskatseid. Proovi uuesti :seconds sekundi pärast.',

    'login' => 'Logi sisse',
    'logout' => 'Logi välja',
    'email' => 'E-posti aadress',
    'password' => 'Parool',
    'register' => 'Registreeri',
    'forgot-password' => 'Unustasid parooli?',
    'remember' => 'Jäta meelde',
    'name' => 'Nimi',
    'confirm-password' => 'Kinnita parool',


];
