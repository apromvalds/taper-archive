<?php
return [
    'title' => 'Pealkiri',
    'content' => 'Sisu',
    'addnew' => 'Lisa postitus',
    'submit' => 'Salvesta',
    'edit' => 'Muuda',
    'delete' => 'Kustuta',
    'all' => 'Kõik',
    'no-title' => 'Pealkiri puudub',
    'images' => 'Pildid',
    'edit-post' => 'Muuda postitust',
    'add-row' => 'Lisa rida',
    'update' => 'Uuenda',
    'view' => 'Vaata postitust',
    'search' => 'Otsi',
    'search-title' => 'Otsing',
    'filters' => 'Filtrid',
    'print' => 'Prindi',
    'available-languages' => 'Saadavalolevad keeled',
    'posted-at' => 'Postitatud',
    'posted-by' => ', autor',
    'not-found' => 'Kriteeriumitele vastavaid postitusi ei leitud.',
    'not-authorized-to-edit' => 'Selle postituse muutmiseks puuduvad õigused.',
    'not-authorized-to-delete' => 'Selle postituse kustutamiseks puuduvad õigused.',
    'type_specific' => 'Spetsiifilised väljad',
];