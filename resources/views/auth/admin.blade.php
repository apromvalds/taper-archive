@extends('layouts.app')

@section('content')
    <div class="container home">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">Administraatori sätted</div>

                    <div class="card-body">
                        @if($users)
                            <h3>Kasutajate haldamine</h3>
                            <div class="row justify-content-center" id="userManagement">
                            @foreach($users as $user)
                                <form method="post" action="/{{app()->getLocale()}}/admin/deleteuser/{{$user->id}}" class="col-sm-6">
                                    {{ method_field('delete') }}
                                    {{csrf_field()}}
                                        {{$user->name}}<br />
                                        {{$user->email}}<br />
                                        {{$user->roles->first()->name}}<br />
                                    <input type="hidden" name="id" value="{{$user->id}}">
                                    <button type="submit" class="btn btn-danger">kustuta</button>
                                </form>
                            @endforeach
                            </div>
                                {{ $users->links() }}
                        @endif
                    </div>
                </div>
            </div>
        </div>



    </div>
@endsection