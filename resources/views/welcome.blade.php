@extends('layouts.app')

@section('content')

@if(app()->getLocale() === 'en')
    <div class="element element-hero">
        <div class="container">
            <div class="row align-items-center justify-content-between">

                <div class="col-md-6">
                    <h1>Taper Archive</h1>
                    <h4>is a place to share your bamboo flyrod and fly specifications.</h4>

                    <p>
                        Filter flyrods or flies by specified parameters or search.
                    </p>
                    @guest
                        <p>
                        Contribute your own by becoming a user.<br>
                            <a class="btn btn-lg btn-primary" href="{{ route('login', ['locale' => app()->getLocale()]) }}">@lang('auth.login')</a>
                            <a class="btn btn-lg btn-success" href="{{ route('register', ['locale' => app()->getLocale()]) }}">@lang('auth.register')</a>
                        </p>
                    @endguest
                </div>
                <div class="col-md-6">
                    <div class="row justify-content-center type-row">
                        <div class="col-md-8">
                            <div class="type-block" style="background: url(images/flyrod.jpg)">
                                <a href="{{ route('posts.type',  ['locale' => app()->getLocale(), 'type' => 'rods']) }}">
                                    <span class="type-block-text">View flyrods</span>
                                </a>
                            </div>
                            <div class="type-block" style="background: url(images/fly.jpg)">
                                <a href="{{ route('posts.type',  ['locale' => app()->getLocale(), 'type' => 'flies']) }}">
                                    <span class="type-block-text">View flies</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="element element-contribute">
        <div class="container">
            <div class="row">



            </div>
        </div>
    </div>

@elseif(app()->getLocale() === 'et')

    <div class="element element-hero">
        <div class="container">
            <div class="row align-items-center justify-content-between">

                <div class="col-md-6">
                    <h1>Taper Archive</h1>
                    <h4>on koht, kus jagada enda loodud lendõngeritvasid ja putukaid.</h4>

                    <p>
                        Vali sobiv rubriik ning filtreeri kuvatavaid postitusi parameetrite või otsingusõna järgi.
                    </p>
                    @guest
                        <p>
                            Anna oma panus, kasutajaks registreerides ning arhiivi täiendades.<br>
                            <a class="btn btn-lg btn-primary" href="{{ route('login', ['locale' => app()->getLocale()]) }}">@lang('auth.login')</a>
                            <a class="btn btn-lg btn-success" href="{{ route('register', ['locale' => app()->getLocale()]) }}">@lang('auth.register')</a>
                        </p>
                    @endguest
                </div>
                <div class="col-md-6">
                    <div class="row justify-content-center type-row">
                        <div class="col-md-8">
                            <div class="type-block" style="background: url(images/flyrod.jpg)">
                                <a href="{{ route('posts.type',  ['locale' => app()->getLocale(), 'type' => 'rods']) }}">
                                    <span class="type-block-text">Vaata ritvu</span>
                                </a>
                            </div>
                            <div class="type-block" style="background: url(images/fly.jpg)">
                                <a href="{{ route('posts.type',  ['locale' => app()->getLocale(), 'type' => 'flies']) }}">
                                    <span class="type-block-text">Vaata putukaid</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="element element-contribute">
        <div class="container">
            <div class="row">



            </div>
        </div>
    </div>

@endif

@endsection