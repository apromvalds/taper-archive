<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="robots" content="noindex,nofollow">

    <title>{{ config('app.name')}}</title>



    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/summernote.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>

</head>
<body>
    <div id="app">
        <nav id="header" class="navbar navbar-expand-md navbar-dark navbar-laravel justify-content-center">
            <div class="container no-gutters">
                <a class="navbar-brand col-4" href="{{ url('/'. app()->getLocale()) }}">
                    {{ config('app.name') }}
                </a>
                <button class="navbar-toggler" typeLaravel="button" data-toggle="collapse" data-target="#header-items" aria-controls="header-items" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                {{--<button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".header-elements" aria-expanded="false" aria-controls="mainMenu userMenu"><span class="navbar-toggler-icon"></span></button>--}}

                <div id="header-items" class="collapse navbar-collapse col-auto mx-auto row header-items">
                <div class="col-6 mx-auto" id="mainMenu">

                    <ul id="primary-menu" class="navbar-nav justify-content-center">
                        <li class="{{ Request::segment(3) === 'rods' ? 'active' : null }}">
                            <a class="nav-link" href="{{ route('posts.type', ['locale' => app()->getLocale(), 'type' => 'rods']) }}">@lang('app.rods')</a>
                        </li>
                        <li class="{{ Request::segment(3) === 'flies' ? 'active' : null }}">
                        <a class="nav-link" href="{{ route('posts.type',  ['locale' => app()->getLocale(), 'type' => 'flies']) }}">@lang('app.flies')</a>
                        </li>

                    </ul>
                </div>

                <div class="order-3 col-6 d-flex justify-content-end dual-collapse" id="userMenu">

                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <ul class="navbar-nav ml-auto">
                                @if (Request::segments())
                                    @foreach (config('app.locales') as $locale)
                                        <li class="nav-item">
                                            @php
                                            $newlang = Request::segments();
                                            $newlang[0] = $locale;
                                            @endphp
                                            <a class="nav-link"
                                               href="{{'/'.implode('/', $newlang)}}"
                                               @if (app()->getLocale() == $locale) style="font-weight: bold; text-decoration: underline" @endif>{{ strtoupper($locale) }}</a>
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                        </li>
                        <li>
                            <ul class="navbar-nav ml-auto">
                                <!-- Authentication Links -->
                                @guest
                                    <li class="nav-item">
                                        <a class="nav-link btn btn-success" href="{{ route('login', ['locale' => app()->getLocale()]) }}">@lang('auth.login')</a>
                                    </li>
                                @else
                                    <li class="nav-item dropdown">
                                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                            {{ Auth::user()->name }} <span class="caret"></span>
                                        </a>

                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                            <a class="dropdown-item" href="{{ route('home', app()->getLocale()) }}">@lang('app.home')</a>
                                            @if(Auth::user()->hasRole('admin'))
                                            <a class="dropdown-item" href="{{ route('admin', app()->getLocale()) }}">Administraatori sätted</a>
                                            @endif
                                            <a class="dropdown-item" href="{{ route('logout', app()->getLocale()) }}"
                                               onclick="event.preventDefault();
                                                             document.getElementById('logout-form').submit();">
                                                @lang('auth.logout')
                                            </a>

                                            <form id="logout-form" action="{{ route('logout', app()->getLocale()) }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                        </div>
                                    </li>
                                @endguest
                            </ul>
                        </li>
                    </ul>
                </div>
                </div>
            </div>
        </nav>
        @if($errors->any())
            <div class="container notices">
                <div class="row justify-content-center">
                    <div class="col-12">
                        @foreach ($errors->all() as $error)
                            <label class="label label-danger">{{ $error }}</label>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif
        <main class="py-4">
            @yield('content')
        </main>
        <footer id="footer">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <a id="footer-logo" href="/{{app()->getLocale()}}">Taper Archive</a>
                    </div>
                </div>
            </div>

        </footer>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" ></script>
    <script src="{{ asset('js/slick.min.js') }}" defer></script>
    <script src="{{ asset('js/summernote.js') }}" defer></script>
    <script src="{{ asset('js/functions.js') }}" defer></script>
    @yield('scripts')
</body>
</html>
