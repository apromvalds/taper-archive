@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">

            <div class="col-12 single-post">
                    <div class="row mt-1 mb-4 align-items-center post-item">
                        <div class="col-sm-10">
                            <h1>{{$post->title}}</h1>
                            Posted at: <i><small>{{$post->created_at}}</small></i> by
                            <i><small>{{\App\User::where('id', $post->user_id)->first()->name}}</small></i> in
                            <i><small>{{$post->post_type->title}}</small></i>
                        </div>
                        <div class="col-sm-2 button-group">
                            @auth
                                @if ($post->user_id == Auth::user()->id || Auth::user()->hasRole('admin') )
                                    <a class="btn btn-outline-primary w-100" href="/{{app()->getLocale()}}/post/{{$post->id}}/edit">@lang('posts.edit')</a>
                                    <a class="btn btn-outline-danger w-100" href="/post/{{$post->id}}/destroy">@lang('posts.delete')</a>
                                @endif
                            @endauth
                            @guest
                                    <a class="btn btn-outline-secondary w-100" onclick="window.print();return false;">@lang('posts.print')</a>
                            @endguest
                        </div>
                    </div>
                    <div class="row mt-1">
                        <div class="col-12">
                            <div class="row justify-content-between">
                                @if ($images)
                                <div class="col-md-7">
                                @else
                                <div class="col-md-12">
                                @endif
                                    <div class="row">
                                        @if (isset(json_decode($post->type_specific_fields)->stations))
                                    <div class="col-md-7">
                                        @else
                                    <div class="col-12">
                                        @endif
                                            @if ($post->type_specific_fields)
                                                <table class="table mt-2">
                                                    @foreach (json_decode($post->type_specific_fields) as $key => $value)
                                                        @if ($post->post_type->get_type_specific_field($key)->type != 'stations-table' && $value)
                                                            <tr>
                                                                <td class="pl-0 pr-1 w-auto type-parameter-name">
                                                                    {{$post->post_type->get_type_specific_field($key)->name}}
                                                                </td>
                                                                <td class="pl-0">
                                                                    {{$value}}
                                                                </td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                </table>

                                            @endif
                                    </div>
                                @if (isset(json_decode($post->type_specific_fields)->stations))
                                    <div class="col-md-5">
                                        <table class="table mt-2">
                                            <thead>
                                            <tr>
                                                <td class="pl-0 pr-1">Station</td>
                                                <td class="pl-0 pr-1">Dimension (mm)</td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @php($station_counter = 0)
                                            @foreach(json_decode($post->type_specific_fields)->stations as $key => $value)
                                                <tr>
                                                    <td class="py-0 pl-0 pr-1 w-25">
                                                        {{$station_counter}}
                                                    </td>
                                                    <td class="py-0 pl-0 pr-1">
                                                        {{$value->dimension}}
                                                    </td>
                                                </tr>
                                                @php($station_counter += 5)
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                @endif
                                </div>

                                {!!$post->content!!}
                                </div>
                                @if($post->content || $post->type_specific_fields)
                                    <div class="col-md-5">
                                @else
                                    <div class="col-md-12">
                                @endif
                                    @if($images)
                                        <div class="post-slider">
                                            @foreach($images as $image)
                                                <img src="{{$image->path}}">
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                            </div>

                        </div>
                    </div>
            </div>

        </div>
    </div>
@endsection