@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-md-9">
                <div class="row align-items-center no-gutters mb-3">
                    @if (isset($search_inputs))
                    <div class="col-sm-8 h3 page-title">@lang('posts.search-title'): {{$post_type->translateOrDefault()->title }}</div>
                    @else
                    <div class="col-sm-8 h3 page-title">@lang('posts.all') {{$post_type->translateOrDefault()->title }}</div>
                    @endif
                    @auth
                        <div class="col-sm-4 text-right">
                            <a href="/{{app()->getLocale()}}/post-new/{{$post_type->id}}" class="btn btn-primary pull-right">@lang('posts.addnew')</a>
                        </div>
                    @endauth
                </div>
                <div class="row">
                        <div class="col-12 posts-main">
                            @foreach($posts as $post)
                                <div class="row align-items-center post-item">
                                    @auth
                                        @if ($post->user_id == Auth::user()->id || Auth::user()->hasRole('admin') )
                                        <div class="col-sm-9">
                                        @else
                                        <div class="col-sm-12">
                                        @endif
                                    @endauth
                                    @guest
                                    <div class="col-sm-12">
                                    @endguest
                                        @php($translations_array = $post->getTranslationsArray())
                                        @if(isset($translations_array[$locale]))
                                        <a class="row align-items-center post-item-link" href="/{{app()->getLocale()}}/post/{{$post->id}}">
                                        @else
                                        <div class="row align-items-center post-item-link">
                                        @endif
                                            @if($post->photos->first())
                                                <div class="col-3 text-center">
                                                    <img class="post-item-image" src="{{$post->photos->first()->path}}">
                                                </div>
                                                <div class="col-9">
                                            @else
                                                <div class="col-12">
                                            @endif
                                            @php($post_translation = $post->translateOrDefault(app()->getLocale()))
                                            <h3>{{$post_translation->title ?? __('posts.no-title')}}</h3>
                                                @if(!isset($translations_array[$locale]))
                                                @lang('posts.available-languages'):
                                                    @foreach($translations_array as $key => $value)
                                                    <a class="available-langs-link" href="/{{$key}}/post/{{$post->id}}">{{$key}}</a>
                                                @endforeach
                                                    <br />
                                                @endif
                                            <small class="post-meta">@lang('posts.posted-at'): {{$post->created_at}} @lang('posts.posted-by') {{\App\User::where('id', $post->user_id)->first()->name}}</small>
                                            @if ($post->type_specific_fields)
                                                <table class="table mt-2">
                                                    @foreach (json_decode($post->type_specific_fields) as $key => $value)
                                                        @if ($post_type->get_type_specific_field($key)->type != 'stations-table' && $value)
                                                            <tr>
                                                                <td class="p-1 w-50 type-parameter-name">
                                                                    {{$post->post_type->get_type_specific_field($key)->name}}
                                                                </td>
                                                                <td class="p-1 pl-0">
                                                                    {{$value}}
                                                                </td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                </table>
                                            @endif
                                        </div>
                                        @if(isset($translations_array[$locale]))
                                        </a>
                                        @else
                                        </div>
                                        @endif
                                    </div>
                                    @auth
                                        @if ($post->user_id == Auth::user()->id || Auth::user()->hasRole('admin') )
                                            <div class="col-sm-3 button-group text-sm-right text-center">
                                                {{--@if(isset($translations_array[$locale]))--}}
                                                    {{--<a class="btn btn-success w-75" href="/{{app()->getLocale()}}/post/{{$post->id}}">@lang('posts.view')</a>--}}
                                                {{--@endif--}}
                                                <a class="btn btn-sm btn-outline-primary w-75" href="{{route('posts.edit', ['locale' => app()->getLocale(), 'id' => $post->id])}}">@lang('posts.edit')</a>
                                                <a class="btn btn-sm btn-outline-danger w-75" href="{{route('posts.destroy', ['locale' => app()->getLocale(), 'id' => $post->id])}}">@lang('posts.delete')</a>
                                            </div>
                                        @endif
                                    @endauth
                                </div>
                            @endforeach
                            {{ $posts->links() }}
                        </div>
                    </div>
                </div>
                <div class="col-md-3" id="sidebar">
                    <div class="sidebar-inner">
                    <form method="post" id="searchform" action="{{route('posts.search', ['locale' => app()->getLocale(), 'type' => $post_type->slug])}}">
                        {{ csrf_field() }}
                        <h5>@lang('posts.search-title')</h5>
                        <input type="text" class="form-control"
                               @isset($search_inputs['search'])
                               value="{{$search_inputs['search']}}"
                               @endisset
                               name="search">
                        @if ($post_type->type_specific_fields)
                            <h5 class="mt-2">@lang('posts.filters')</h5>
                            @foreach ( json_decode($post_type->type_specific_fields) as $key => $value )
                                @if($value->filterable == true)
                                    @if($value->type == 'select')
                                        <label>
                                            {{$value->name}}
                                            <select name="{{$key}}" class="form-control">
                                                <option style="display:none">
                                                @foreach($value->options as $select_value => $select_text)
                                                    <option
                                                            @if(isset($search_inputs[$key]) && $search_inputs[$key] === $select_value)
                                                                selected
                                                            @endif
                                                            value="{{$select_value}}">{{$select_text}}</option>
                                                @endforeach
                                            </select>
                                        </label>
                                    @else
                                        <label>
                                            {{$value->name}}
                                            <input type="{{$value->type}}"
                                                    @isset($search_inputs[$key])
                                                        value="{{$search_inputs[$key]}}"
                                                    @endisset
                                                   class="form-control mb-1" placeholder="{{$value->name}}" name="{{$key}}" id="{{$key}}">
                                        </label>
                                    @endif
                                @endif
                            @endforeach
                        @endif
                        <button type="submit" class="btn btn-outline-primary mt-2">@lang('posts.search')</button>
                    </form>
                    </div>
                </div>
        </div>
    </div>
@endsection