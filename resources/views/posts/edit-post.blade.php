@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">@lang('posts.edit-post')</div>
                    <form id="add-form" class="card-body" method="post" action="/{{app()->getLocale()}}/post/{{$post->id}}/edit"  enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')
                            <div class="row">
                            <div class="col-12 form-group post-edit">
                                    <div class="form-group row mt-1 mb-1 post-item">
                                        <div class="col-lg-10 col-md-6 col-12">
                                            <h5>@lang('posts.title')</h5>
                                            <h1><input name="title" class="form-control" type="text" value="{{$post->title}}" required></h1>
                                            Posted at: <i><small>{{$post->created_at}}</small></i> by
                                            <i><small>{{\App\User::where('id', $post->user_id)->first()->name}}</small></i> in
                                            <i><small>{{$post->post_type->title}}</small></i>
                                        </div>
                                        <div class="col-lg-2 col-md-6 col-12 mt-md-0 mt-3 button-group">
                                            @auth
                                                @if ($post->user_id == Auth::user()->id || Auth::user()->hasRole('admin') )
                                                    <button type="submit" class="btn btn-success w-100" >@lang('posts.update')</button>
                                                    <a href="/{{app()->getLocale()}}/post/{{$post->id}}" class="btn btn-outline-primary w-100" >@lang('posts.view')</a>
                                                    <a class="btn btn-outline-danger w-100" href="/{{app()->getLocale()}}/post/{{$post->id}}/destroy">@lang('posts.delete')</a>
                                                @endif
                                            @endauth
                                        </div>
                                    </div>
                                    <div class="form-group row mt-1">
                                        @if ($type->type_specific_fields)
                                            <div class="col-12 mt-4">
                                                <h5>@lang('posts.type_specific')</h5>
                                            </div>
                                            <div id="type_specific" class="col-12">
                                                @php
                                                $post_values = !$post->type_specific_fields ?: json_decode($post->type_specific_fields);
                                                @endphp
                                                @foreach ( json_decode($type->type_specific_fields) as $key => $value )
                                                    @if($value->type == 'select')
                                                        <label>
                                                            {{$value->name}}
                                                            <select name="{{$key}}" class="browser-default custom-select">
                                                                @foreach($value->options as $select_value => $select_text)
                                                                    <option
                                                                            @if($post_values->$key == $select_value)
                                                                                    selected
                                                                            @endif
                                                                            value="{{$select_value}}">{{$select_text}}</option>
                                                                @endforeach
                                                            </select>
                                                        </label>
                                                    @elseif ($value->type == 'stations-table')
                                                        <label class="w-100">
                                                        {{$value->name}}
                                                            <table id="stations-table" class=" table order-list">
                                                                <thead>
                                                                <tr>
                                                                    <td>Station</td>
                                                                    <td>Dimension (mm)</td>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                @if ($post_values->stations)
                                                                    @php($station_counter = 0)
                                                                    @foreach($post_values->stations as $key => $value)
                                                                        <tr>
                                                                            <td class="col-4 w-25">
                                                                                <input type="hidden" value="{{$station_counter}}" name="station" >
                                                                                {{$station_counter}}
                                                                            </td>
                                                                            <td class="col-4">
                                                                                <input type="number" step="0.001" name="dimension" value="{{$value->dimension}}" class="form-control"/>
                                                                            </td>
                                                                            <td class="col-sm-2"><a class="deleteRow"></a>

                                                                            </td>
                                                                        </tr>
                                                                        @php($station_counter += 5)
                                                                    @endforeach
                                                                @else
                                                                <tr>
                                                                    <td class="col-4 w-25">
                                                                    <input type="hidden" value="0" name="station" >
                                                                    0
                                                                    </td>
                                                                    <td class="col-4">
                                                                        <input type="number" step="0.001" name="dimension"  class="form-control"/>
                                                                    </td>
                                                                    <td class="col-sm-2"><a class="deleteRow"></a>

                                                                    </td>
                                                                </tr>
                                                                @endif
                                                                </tbody>
                                                                <tfoot>
                                                                <tr>
                                                                    <td colspan="5" style="text-align: left;">
                                                                        <input type="button" class="btn btn-lg btn-block " id="addrow" value="@lang('posts.add-row')" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                </tr>
                                                                </tfoot>
                                                            </table>
                                                        </label>
                                                    @else
                                                        <label>
                                                            {{$value->name}}
                                                            <input type="{{$value->type}}" value="{{$post_values->$key}}" class="form-control mb-1" placeholder="{{$value->name}}" name="{{$key}}" id="{{$key}}">
                                                        </label>
                                                    @endif
                                                @endforeach
                                            </div>
                                        @endif
                                        <div class="form-group col-12 mt-4">
                                            <label>
                                                <h5>@lang('posts.images')</h5>
                                                <input type="file" name="images[]" class="form-control" multiple accept="image/*">
                                            </label>
                                        </div>

                                        <div class="col-12 mt-4">
                                            <h5>@lang('posts.content')<h5>
                                            <textarea name="content" id="content" class="summernote">{!! $post->content !!}</textarea>
                                        </div>
                                    </div>
                            </div>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        ( function( $ ) {
            $('.summernote').summernote('code', {!! json_encode($post->content) !!});
        });
    </script>
@endsection