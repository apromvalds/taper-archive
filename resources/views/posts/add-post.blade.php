@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">{{__('posts.addnew')}}</div>

                    <form class="card-body" id="add-form" method="post" action="/{{app()->getLocale()}}/post-new" enctype="multipart/form-data">
                        {{ method_field('post') }}
                        {{csrf_field()}}
                        <div class="row">
                        <div class="col-12 post-add">
                            <div class="row mt-1 mb-1 align-items-center post-item">
                                <div class="col-lg-10 col-md-6 col-12">
                                    <h5>{{__('posts.title')}}</h5>
                                    <input name="title" type="text" class="form-control" id="title" placeholder="{{__('posts.title')}}" required>
                                </div>
                                <div class="col-lg-2 col-md-6 col-12 mt-md-0 mt-3 button-group">
                                    <input type="submit" value="{{__('posts.submit')}}" class="btn btn-success w-100">
                                </div>
                            </div>
                            <div class="form-group row">
                                @if ($type->type_specific_fields)
                                    <div class="col-12 mt-4">
                                        <h5>{{__('posts.type_specific')}}</h5>
                                    </div>
                                    <div id="type_specific" class="col-12">
                                        @foreach ( json_decode($type->type_specific_fields) as $key => $value )
                                            @if($value->type == 'select')
                                                <label>
                                                    {{$value->name}}
                                                    <select name="{{$key}}" class="form-control">
                                                        @foreach($value->options as $select_value => $select_text)
                                                        <option value="{{$select_value}}">{{$select_text}}</option>
                                                        @endforeach
                                                    </select>
                                                </label>
                                            @elseif ($value->type == 'stations-table')
                                                <label class="w-100">
                                                    {{$value->name}}
                                                    <table id="stations-table" class=" table order-list">
                                                        <thead>
                                                        <tr>
                                                            <td>Station</td>
                                                            <td>Dimension</td>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td class="col-4 w-25">
                                                                <input type="hidden" value="0" name="station" >
                                                                0
                                                            </td>
                                                            <td class="col-4">
                                                                <input type="number" step="0.001" name="dimension"  class="form-control"/>
                                                            </td>
                                                            <td class="col-sm-2"><a class="deleteRow"></a>

                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                        <tfoot>
                                                        <tr>
                                                            <td colspan="5" style="text-align: left;">
                                                                <input type="button" class="btn btn-lg btn-block " id="addrow" value="@lang('posts.add-row')" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                        </tr>
                                                        </tfoot>
                                                    </table>
                                                </label>
                                            @else
                                                <label>
                                                    {{$value->name}}
                                                <input type="{{$value->type}}" class="form-control mb-1" placeholder="{{$value->name}}" name="{{$key}}" id="{{$key}}">
                                                </label>
                                            @endif
                                        @endforeach
                                    </div>
                                @endif
                                <div class="form-group col-12 mt-4">
                                    <label>
                                        <h5>@lang('posts.images')</h5>
                                        <input type="file" name="images[]" class="form-control" multiple accept="image/*">
                                    </label>
                                </div>

                                <div class="col-12 mt-4">
                                    <h5>{{__('posts.content')}}</h5>
                                    <textarea name="content" id="content" class="summernote"></textarea>
                                </div>

                                <input type="hidden" name="type" value="{{$type->id}}">
                            </div>
                        </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection