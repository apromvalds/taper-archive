@extends('layouts.app')

@section('content')
<div class="container home">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
                <div class="card-header">@lang('home.my-user')</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </div>

    @if ($posts && $posts->first())
        <div class="row mt-3 justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">@lang('home.my-posts')</div>

                    <div class="card-body">
                        <div class="col-12 posts-main">
                            @foreach($posts as $post)
                                <div class="row align-items-center post-item">
                                    @auth
                                        @if ($post->user_id == Auth::user()->id || Auth::user()->hasRole('admin') )
                                            <div class="col-10">
                                        @else
                                            <div class="col-12">
                                        @endif
                                    @endauth
                                    @guest
                                        <div class="col-12">
                                    @endguest

                                    <a class="post-item-link" href="/{{app()->getLocale()}}/post/{{$post->id}}">
                                        @php($post_translation = $post->translateOrDefault(app()->getLocale()))
                                        <h3>{{$post_translation->title ?? __('posts.no-title')}}</h3>
                                        <small class="post-meta">@lang('posts.posted-at'): {{$post->created_at}} @lang('posts.posted-by') {{\App\User::where('id', $post->user_id)->first()->name}}</small>
                                        @if ($post->type_specific_fields)
                                            <table class="table mt-2">
                                                @foreach (json_decode($post->type_specific_fields) as $key => $value)
                                                    @if ($post->post_type->get_type_specific_field($key)->type != 'stations-table' && $value)
                                                        <tr>
                                                            <td class="p-1 w-25 type-parameter-name">
                                                                {{$post->post_type->get_type_specific_field($key)->name}}
                                                            </td>
                                                            <td class="p-1 pl-0">
                                                                {{$value}}
                                                            </td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            </table>
                                        @endif
                                    </a>
                                    </div>
                                    @auth
                                        @if ($post->user_id == Auth::user()->id || Auth::user()->hasRole('admin') )
                                            <div class="col-2 text-center">
                                                <a class="btn btn-outline-primary w-100" href="{{route('posts.edit', ['locale' => app()->getLocale(), 'id' => $post->id])}}">@lang('posts.edit')</a>
                                                <a class="btn btn-outline-danger w-100" href="{{route('posts.destroy', ['locale' => app()->getLocale(), 'id' => $post->id])}}">@lang('posts.delete')</a>
                                            </div>
                                        @endif
                                    @endauth
                                </div>
                            @endforeach
                        </div>
                    </div>

                </div>
            </div>
        </div>
    @endif

</div>
@endsection