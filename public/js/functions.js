
window.onerror = function(message, url, lineNo) {
    return false;
};
$('#add-form').submit(function () {
    var type_specific_json = {};

    if ($('#type_specific #stations-table').length) {
        type_specific_json['stations'] = {};
        var station_row = 0;
        $('#type_specific #stations-table tr').each(function () {
            $(this).children().find('input[type="number"]').each(function () {
                type_specific_json['stations'][station_row] = {};
                type_specific_json['stations'][station_row][$(this).attr('name')] = $(this).val();
            });
            station_row++;
            $(this).remove();
        });
    }

    $('#type_specific input, #type_specific select').each(function () {
        var name = $(this).attr('name');
        var value = $(this).val();
        if ($(this).attr('type') === 'number' && $(this).val().length){
            value = parseInt(value);
        }
        type_specific_json[name] = value;
    });

    var $hidden = $("<input type='hidden' name='type_specific_fields'>");
    $hidden.val(JSON.stringify(type_specific_json));
    $(this).append($hidden);
    return true;
});

//table input
var table_counter = 0;
var station_number = 0;

$("#addrow").on("click", function () {
    station_number = parseInt($('#stations-table tbody > tr:last-child').children().find('input[type="hidden"]').val()) + 5;
    var newRow = $("<tr>");
    var cols = "";

    cols += '<td><input type="hidden" value="'+ station_number +'" name="station'+ table_counter +'" >'+ station_number +'</td>';
    cols += '<td><input type="number" step="0.001" class="form-control" name="dimension"/></td>';
    cols += '<td><input type="button" class="ibtnDel btn btn-md btn-danger "  value="X"></td>';
    newRow.append(cols);
    $("table.order-list").append(newRow);
    table_counter++;
});

$("table.order-list").on("click", ".ibtnDel", function (event) {
    $(this).closest("tr").remove();
    table_counter -= 1
});

function calculateRow(row) {
    var price = +row.find('input[name^="price"]').val();

}

function calculateGrandTotal() {
    var grandTotal = 0;
    $("table.order-list").find('input[name^="price"]').each(function () {
        grandTotal += +$(this).val();
    });
    $("#grandtotal").text(grandTotal.toFixed(2));
}

//slider

$('.post-slider').slick({
    dots: false,
    infinite: true,
    speed: 500,
    fade: true,
    cssEase: 'linear',
    nextArrow: '<button type="button" class="slick-arrow slick-next"><span>&gt;</span></button>',
    prevArrow: '<button type="button" class="slick-arrow slick-prev"><span>&lt;</span></button>',
});

$('.summernote').summernote();
$('.note-toolbar .note-fontsize, .note-fontname, .note-insert, .note-group-select-from-files, .note-view, .note-toolbar .note-color, .note-toolbar .note-para .dropdown-menu li:first, .note-toolbar .note-line-height').remove();